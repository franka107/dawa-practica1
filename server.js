const express = require('express')
const app = express()
const port = process.env.PORT || 3000;

//Declarando express
const hbs = require('hbs')
hbs.registerPartials(__dirname + '/views/partials')

app.use(express.static(__dirname + '/public'))
app.set('view engine', 'hbs')

app.get('/', function (req, res) {
  res.render('home')
})

app.get('/servicios', function (req, res) {
  res.render('services')
})

app.get('/nosotros', function (req, res) {
  res.render('we')
})

app.listen(port, () => {
  console.log(`escuchando peticiones en el puerto ${port}`)
})